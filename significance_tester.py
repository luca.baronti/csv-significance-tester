#!/usr/bin/env python3

###############
# Code developed by Luca Baronti
# Please refer to the repository for furthoer information 
# https://gitlab.com/luca.baronti/csv-significance-test
###############

import sys, argparse
import numpy as np
from scipy.stats import mannwhitneyu, wilcoxon, kruskal, friedmanchisquare

def generateExample(n_samples=10000):
	f=open("example.csv",'w')
	f.write("N(0;1),N(0;1),N(.1;1)\n")
	np.random.seed()
	norm1=np.random.normal(loc=.0, scale=1.0, size=n_samples)
	norm2=np.random.normal(loc=.0, scale=1.0, size=n_samples) 
	norm3=np.random.normal(loc=.10, scale=1.0, size=n_samples)
	for i in range(n_samples):
		f.write(','.join([str(x) for x in [norm1[i], norm2[i], norm3[i]]]) + '\n')
	f.close()

def determineDelimiter(path):
	f=open(path)
	line=f.readline().strip()
	f.close()
	d=','
	if len(line.split(d))==1:
		d='\t'
	else:
		return d
	if len(line.split(d))==1:
		d=' '
	else:
		return d
	if len(line.split(d))==1:
		raise ValueError("Can't determine the delimiter in file "+path)
	else:
		return d

def readHeader(path,delimiter=','):
	f=open(path)
	header=f.readline().strip().split(delimiter)
	try:
		float(header[0])
		header=[]
	except ValueError:
		pass
	f.close()
	return header

def computeMannWhitney(data1, data2):
	stat, p_value = mannwhitneyu(data1, data2, alternative="two-sided")
	return stat, p_value

def computeWilcoxon(data1, data2):
	stat, p_value = wilcoxon(data1, data2)
	return stat, p_value

def computeKruskal(data):
	stat, p_value = kruskal(*[data[:,i] for i in range(len(data[0,:]))])
	return stat, p_value

def computeFriedman(data):
	stat, p_value = friedmanchisquare(*[data[:,i] for i in range(len(data[0,:]))])
	return stat, p_value

def acceptORrejectP(p,threshold=0.05):
	if p<=threshold:
		return "Samples distribution is likely DIFFERENT (p<="+str(threshold)+")"
	else:
		return "Samples distribution is likely the same (p>"+str(threshold)+")"

def print_result(u, p, prefix, full_decimal, only_p, print_stats, alpha):
	if full_decimal:
		p_str=str(p)
		u_str=str(u)
	else:
		p_str="%.5f"%p
		u_str="%.5f"%u
	if only_p:
		print(p_str)
	else:
		if prefix!="":
			if print_stats:
				print(prefix,"U:",u_str,"P:",p_str,acceptORrejectP(p, threshold=alpha), sep='\t')
			else:
				print(prefix,p_str,acceptORrejectP(p, threshold=alpha), sep='\t')
		else:
			if print_stats:
				print("U:",u_str,"P:",p_str,acceptORrejectP(p, threshold=alpha), sep='\t')
			else:
				print(p_str,acceptORrejectP(p, threshold=alpha), sep='\t')

if __name__=="__main__":
	parser = argparse.ArgumentParser(description="This tool can perform different type of significance tests on data present in a CSV file. \
																								The tests aim to discard the null hyphothesis (i.e. that the given sampoles came from different distributions). \
																								The samples must be arranged column-wise. Optionally an header can be present in the first line.\
																								As default, the Mann-Whitney U test is used.")
	parser.add_argument('csv_file', metavar='csv_file', type=str, 
                    help='Comma Separated Values file containing the data to be plotted, other kind of separators (e.g tabs) may also be accepted.')
	parser.add_argument('-p',  action='store_true',
                    help="Prints only the p-values, one per line. It's useful in case the output need to be parsed by a script.")
	parser.add_argument('-u',  action='store_true',
                    help="Prints also the statistic value.")
	parser.add_argument('-alpha', metavar='alpha_value', type=float, default=0.05,
                    help='Set the confidence interval used to discard the null hypothesis.')
	parser.add_argument('--full_decimal',  action='store_true',
                    help="Prints the p-values using more decimal numbers (default is 5).")
	parser.add_argument('--wilcoxon',  action='store_true',
                    help="Use the Wilcoxon Signed-Rank test. This test is usually better when the samples are not independent \
                    			(e.g. algorithm's results tested on different datasets, different algorithms' results tested on the same dataset).")
	parser.add_argument('--kruskal',  action='store_true',
                    help="Use the Kruskal-Wallis H test, under the assumption that the samples are independent. \
                    			Similar to the Mann-Whitney U test, it is used when many samples (i.e. columns) are present, \
                    			and you are only interested if at least two of them came from different distribution. \
                    			It's more efficient, but it can't identify which samples are different.")
	parser.add_argument('--friedman',  action='store_true',
                    help="Use the Friedman test, under the assumption that the samples are NOT independent. \
                    			Similar to the Wilcoxon Signed-Rank test, it is used when many samples (i.e. columns) are present, \
                    			and you are only interested if at least two of them came from different distribution. \
                    			It's more efficient, but it can't identify which samples are different.")
	args = parser.parse_args()
	generateExample()
	csv_file=args.csv_file
	delimiter=determineDelimiter(csv_file)
	header=readHeader(csv_file, delimiter)
	# read data
	if len(header)==0:
		data = np.genfromtxt(csv_file, delimiter=delimiter)
	else:
		data = np.genfromtxt(csv_file, skip_header=1, delimiter=delimiter)
	if len(data)==0:
		sys.stderr.write("CSV file "+csv_file+" empty\n")
		sys.exit(1)
	n_columns=len(data[0,:])
	if n_columns<2:
		sys.stderr.write("CSV file "+csv_file+" contains only one column\n")
		sys.exit(1)
	if len(header)==0:
		header=["c"+str(i) for i in range(n_columns)]
	# Compute Tests
	if args.kruskal:
		u, p = computeKruskal(data)
		print_result(u, p, prefix="", full_decimal=args.full_decimal, only_p=args.p, print_stats=args.u, alpha=args.alpha)
	elif args.friedman:
		u, p = computeFriedman(data)
		print_result(u, p, prefix="", full_decimal=args.full_decimal, only_p=args.p, print_stats=args.u, alpha=args.alpha)
	else:
		for i in range(n_columns):
			for j in range(n_columns)[i+1:]:
				if args.wilcoxon:
					u, p = computeWilcoxon(data[:,i], data[:,j])
				else:
					u, p = computeMannWhitney(data[:,i], data[:,j])
				print_result(u, p, prefix=header[i]+'/'+header[j]+':', full_decimal=args.full_decimal, only_p=args.p, print_stats=args.u, alpha=args.alpha)
