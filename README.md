# CSV Significance Test

This tool can perform different type of significance tests on data present in a CSV file.
The tests aim to discard the null hyphothesis (i.e. that the given samples came from different distributions) 
The samples must be arranged column-wise. Optionally an header can be present in the first line.

The tool is wrote in Python 3 and it uses the *numpy* and *scipy* modules.

# Usage
Having a file *example.csv* containing (e.g.) samples that came from the following normal distributions: N(0,1), N(0,1) and N(0.1,1) arranged column-wise, the following command:
```sh
$ ./significance_tester.py example.csv
```
will identify which samples are likely to came from the same distributions:
```
N(0;1)/N(0;1):	0.33546	Samples distribution is likely the same (p>0.05)
N(0;1)/N(.1;1):	0.00000	Samples distribution is likely DIFFERENT (p<=0.05)
N(0;1)/N(.1;1):	0.00000	Samples distribution is likely DIFFERENT (p<=0.05)
```
Use the flag **-h** to print an helper with detailed and updated info:
```sh
$ ./significance_tester.py -h
```
# Tests available
As default, the two-sided [Mann-Whitney U](https://en.wikipedia.org/wiki/Mann%E2%80%93Whitney_U_test) test is used, with threshold $`\alpha=0.05`$.
Alternatively, the following tests can be used:

- **wilcoxon** Use the [Wilcoxon Signed-Rank](https://en.wikipedia.org/wiki/Wilcoxon_signed-rank_test) test. This test is usually better when the samples are not independent (e.g. algorithm's results tested on different datasets, different algorithms' results tested on the same dataset).
- **kruskal**  Use the [Kruskal-Wallis H](https://en.wikipedia.org/wiki/Kruskal%E2%80%93Wallis_one-way_analysis_of_variance) test, under the assumption that the samples are independent. Similar to the Mann-Whitney U test, it is used when many samples (i.e. columns) are present, and you are only interested if at least two of them came from different distribution. It's more efficient, but it can't identify which samples are different.
- **friedman** Use the [Friedman test](https://en.wikipedia.org/wiki/Friedman_test), under the assumption that the samples are NOT independent. Similar to the Wilcoxon Signed-Rank test, it is used when many samples (i.e. columns) are present, and you are only interested if at least two of them came from different distribution. It's more efficient, but it can't identify which samples are different.

Further utility flags can also be set, like **-p** which prints only the p-values, one per line. It's useful in case the output need to be parsed by a script.
The the confidence interval $`\alpha`$ can be set wit the flag **-alpha**, although this is only used in the printed text.
